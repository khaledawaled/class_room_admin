
function add_student(student_id, class_room_id) {
    $.ajax({
      url: "/add_student",
      type: 'PUT',
      data: {
        student_id : student_id,
        class_room_id : class_room_id
      }
    }).done(function(msg)
            {
                alert( "Data Saved" );
                window.location.reload();
            })
} 

function delete_student(student_id, class_room_id) {
  $.ajax({
    url: "/delete_student",
    type: 'PUT',
    data: {
      student_id : student_id,
      class_room_id : class_room_id
    }
  }).done(function(msg)
          {
              alert( "Data Saved" );
              window.location.reload();
          })
} 