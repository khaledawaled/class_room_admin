class User < ApplicationRecord
    has_and_belongs_to_many :class_rooms
    has_secure_password
    validates :email, presence: true    
    
end
