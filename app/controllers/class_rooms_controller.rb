class ClassRoomsController < ApplicationController
    skip_before_action :verify_authenticity_token
    before_action :student_user, only: [:new, :create, :show, :destroy, :update]
    def new
        @class_room = ClassRoom.new
    end
    def create
        @class_room = ClassRoom.create(class_room_params)
        add_instructor if @class_room.save

    end
    def show
        @class_room = ClassRoom.find(params[:id])
        @unassigned_students = unassigned_students_query
        @class_room_students = class_room_students_query
        @class_room_instructor = current_user
    end
    def update
    end
    def destroy
        @class_room = ClassRoom.find(params[:id])
        @class_room.users.delete_all
        ClassRoom.delete(@class_room)
        redirect_to dashboard_path
    end
    def add_student
        @student = User.find(params[:student_id])
        @class_room = ClassRoom.find(params[:class_room_id])
        #for debug
        @class_room.users << @student 
        #render '/show' if @class_room.save
    end
    def delete_student
        @student = User.find(params[:student_id])
        @class_room = ClassRoom.find(params[:class_room_id])
        @class_room.users.delete(@student)
    end
    private
    def unassigned_students_query
        User.joins("LEFT JOIN class_rooms_users ON users.id = class_rooms_users.user_id")
            .where("class_room_id IS NULL and type ='Student' ")
    end

    def class_room_students_query
        User.joins("INNER JOIN class_rooms_users ON users.id = class_rooms_users.user_id")
        .where("type ='Student' and class_room_id = #{@class_room.id} ")
    end
    def add_instructor
        @instructor = current_user
        @class_room.users << @instructor
        redirect_to dashboard_path
    end
    def class_room_params
        params.require(:class_room).permit(:name)
    end
    def student_user
        @user = current_user
        
        if @user.type == "Student"
            flash[:danger] = "only Instructors can access class rooms"
            redirect_to dashboard_path 
        end
    end
end
