class DashboardsController < ApplicationController
    before_action :logged_in_user, only: [:index]
    def index
        @user = current_user
        if @user.type == 'Student'
            student_dashboard
        else
            instructor_dashboard
        end
    end
    private
    def logged_in_user
        unless logged_in?
            flash[:danger] = "please log in."
            redirect_to login_path
        end
    end
    def student_dashboard
        # check if user.classrooms is empty or not 
        @class_room = @user.class_rooms.first if !@user.class_rooms.empty?
        render :student_dashboard
    end
    def instructor_dashboard
        @class_rooms = @user.class_rooms
        render :instructor_dashboard
    end
end
