class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by(email: params[:session][:email])
    if user && user.authenticate(params[:session][:password])
      log_in user
      redirect_to dashboard_path
    else
      flash[:danger] = "Email or password is invalid"
      render "new"
    end
  end

  def destroy
    log_out
    redirect_to root_url, notice: "Logged out!"
  end
end
