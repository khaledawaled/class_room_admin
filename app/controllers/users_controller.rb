class UsersController < ApplicationController
    def new
        @user = User.new
    end

    def create
        @user = User.new(user_params)
        if @user.save
            log_in @user
            #flash[:success] = "Welcome #{@user.type} #{@user.email} to the class room portal app"
            redirect_to '/dashboard'
        else 
            render 'new'
        end
    end
    private
    def user_params
        params.require(:user).permit(:email, :password, :type)
    end
end
