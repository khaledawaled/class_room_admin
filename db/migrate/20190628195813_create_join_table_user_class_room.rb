class CreateJoinTableUserClassRoom < ActiveRecord::Migration[5.2]
  def change
    create_join_table :class_rooms, :users do |t|
       t.index [:class_room_id, :user_id]
    end
  end
end
