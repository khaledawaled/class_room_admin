class AddNameToClassRoom < ActiveRecord::Migration[5.2]
  def change
    add_column :class_rooms, :name, :string
  end
end
