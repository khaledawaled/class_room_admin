class AddUniqeToClassRoomUserTable < ActiveRecord::Migration[5.2]
  def change
    remove_index :class_rooms_users, [:class_room_id, :user_id]
    add_index :class_rooms_users, [:class_room_id, :user_id], unique: true
  end
end
