require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the SessionsHelper. For example:
#
# describe SessionsHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe SessionsHelper, type: :helper do
  context 'login user' do
    it "should save user id in session id" do
      user = User.new(email: "aa@live.com", password: "123")
      log_in user
      expect(session[:user_id]).to eql(user.id)
    end
  end
end
