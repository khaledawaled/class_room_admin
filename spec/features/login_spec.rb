require 'rails_helper'

RSpec.feature "Login", type: :feature do
  describe "GET /login" do
    context " Student " do
      it " create instance and redirect to dashboard capybara" do
        user = User.create(:email => "aaa@live.com", :password => "123", :type => "Student")
        visit "/login"
        fill_in "session[email]", :with => "aaa@live.com"
        fill_in "session[password]", :with => "123"
        click_button "commit"
        expect(page).to have_text("Student")
      end
    end
    context " Instructor " do
      it " create instance and redirect to dashboard capybara" do
        user = User.create(:email => "aaa@live.com", :password => "123", :type => "Student")
        visit "/login"
        fill_in "session[email]", :with => "aaa@live.com"
        fill_in "session[password]", :with => "123"
        click_button "commit"
        expect(page).to have_text("Student")
      end
    end
  end
end
