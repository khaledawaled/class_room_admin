require 'rails_helper'

RSpec.describe "Login", type: :request do
  describe "GET /login" do
    it " login user and redirect to dashboard" do
      user = User.create(:email => "aaa@live.com", :password => "123")
      post '/login', :params => {:session => { :email => "aa@live.com", :password => "123" } }
      expect(response).to redirect_to(dashboard_path)
      
    end
  end
end
