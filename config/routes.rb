Rails.application.routes.draw do
  root 'home#index'
  get 'home/index'
  get '/login' ,to: 'sessions#new'
  post '/login',to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  get '/signup', to:'users#new'
  post '/signup', to: 'users#create'
  resources :users, only: [:new, :create]
  resources :class_rooms
  get '/dashboard', to: 'dashboards#index'
  put 'add_student', to: 'class_rooms#add_student'
  put 'delete_student', to: 'class_rooms#delete_student'
end
